This is a simple developer-oriented module to make creating Display Suite function-based fields in code a little quicker.

It basically wraps Display Suite's functional API in a semi-Object Oriented wrapper.

You shouldn't need to install this unless you are a developer interested in its functionality, or requested by another module.

Usage
<?php
/**
 * Implements hook_ds_fields_info().
 *
 * The main function is \Drupal\ds_fp\Field\Field::field(), which will genenerate
 * all the code you need for a basic function field, with defaults set.
 *
 * Overriding individual properties is relatively easy.
 *
 * @see ds_get_fields()
 */
function HOOK_ds_fields_info($entity_type) {
  $fields = array();

  // Add a new field called "my_field" using "my_field_callback".
  $fields['foo'] = \Drupal\ds_fp\Field\Field::field('example_module', 'my_field', 'my_field_callback');

  // Set the title on "my_field".
  $fields['foo']->setTitle('A new field title');

  return array('node' => $fields);
}

/**
 * Field callback to render an entity byline.
 */
function example_module_my_field_callback($vars) {
  // Do something with $vars...

  return $output;
}
?>