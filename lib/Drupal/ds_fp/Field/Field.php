<?php
/**
 * @file
 * The main Display Suite Field controller class.
 *
 * @copyright Copyright(c) 2012 Christopher Skene
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 */

namespace Drupal\ds_fp\Field;

/**
 * Class Field
 * @package Drupal\ds_fp\Field
 */
class Field {

  /**
   * Factory function to load a field prototype.
   *
   * @param string $module
   *   The module requesting the field.
   * @param string $machine_name
   *   The machine name for the field.
   * @param int $type
   *   (optional) The field type.
   *
   * @return Prototype
   *   A field prototype.
   */
  static public function prototype($module, $machine_name, $type = DS_FIELD_TYPE_FUNCTION) {

    $field = new Prototype($module, $machine_name, $type);

    return $field;
  }

  /**
   * Factory function to create a full field with title.
   *
   * Useful when you don't need anything but the basics. This will create a
   * function field which calls a function called:
   * MODULE_MACHINE-NAME_field.
   *
   * @param string $module
   *   The module requesting the field.
   * @param string $machine_name
   *   The machine name for the field.
   * @param string $name
   *   The field name.
   *
   * @return Prototype
   *   A field prototype.
   */
  static public function field($module, $machine_name, $name) {

    $field = new Prototype($module, $machine_name, DS_FIELD_TYPE_FUNCTION);
    $field->setTitle($name);

    return $field->field();
  }

  /**
   * Helper to make a tag.
   *
   * @param string $element
   *   The type of element to generate.
   * @param string $content
   *   The content
   * @param array $attributes
   *   (optional) An array of attributes.
   *
   * @return string
   *   A formatted tag.
   *
   * @see drupal_attributes()
   */
  static public function createTag($element, $content, $attributes = array()) {

    if (!empty($attributes)) {
      $attr = ' ' . drupal_attributes($attributes);
    }
    else {
      $attr = '';
    }

    return '<' . $element . $attr . '>' . $content . '</' . $element . '>';
  }
}